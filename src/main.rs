use std::{
    fs::{self, DirEntry},
    io::Write,
    path::PathBuf,
};

fn main() {
    let mut paths: Vec<DirEntry> = fs::read_dir("input/")
        .expect("Failed to read directory contents")
        .map(|entry| entry.expect("Failed to read directory entry"))
        .collect();
    paths.sort_by_key(|dir| dir.path());

    let mut output = std::fs::File::create("output.html").expect("create failed");
    output.write_all("<!DOCTYPE html>\n<html>\n<head>\n<link rel=\"stylesheet\" href=\"styles.css\">\n</head>\n<body>".as_bytes()).expect("write failed");
    for file in paths {
        if file.file_type().unwrap().is_file() {
            output
                .write_all(proccess_file(file.path()).as_bytes())
                .expect("write died");
        }
    }
    output
        .write_all("</body>\n</html>".as_bytes())
        .expect("write died");
}

fn proccess_file(path: PathBuf) -> String {
    let mut ret = String::from("");

    let path_str = path.as_path().display().to_string();
    let contents = fs::read_to_string(path).expect(&format!("Cant read file: {}", path_str));
    let trimmed = contents.trim();
    if trimmed == "" {
        return ret;
    }

    let splited: Vec<&str> = trimmed.split("\n").collect();
    let (i1, i2, maybe_i3) = find_bouderies(&splited);

    ret += &format!("<h1>{}</h1>\n", splited[0]);
    for i in i1..=i2 {
        if only_white_spaces(splited[i]) {
            ret += "<br>\n";
        } else {
            ret += &format!("{}<br>\n", format_normal_line(splited[i]))
        }
    }
    if maybe_i3 == None {
        return ret;
    }

    ret += "<br>\n<pre>\n";
    for i in maybe_i3.unwrap()..splited.len() {
        ret += &format! {"{}\n",splited[i]};
    }

    return ret + "</pre>\n";
}

fn find_bouderies(splited: &Vec<&str>) -> (usize, usize, Option<usize>) {
    let mut i1 = splited.len();
    let mut i2 = splited.len() - 1;
    let mut i3 = None;

    // 1 - finding first, 2-finding line, 3-finding first under line
    let mut state = 1;
    for i in 1..splited.len() {
        match state {
            1 => {
                if !only_white_spaces(splited[i]) {
                    i1 = i;
                    i2 = i;
                    state = 2;
                }
            }
            2 => {
                if splited[i].starts_with("---") {
                    state = 3;
                } else if !only_white_spaces(splited[i]) {
                    i2 = i;
                }
            }
            3 => {
                if !only_white_spaces(splited[i]) {
                    i3 = Some(i);
                    break;
                }
            }
            _ => panic!("aaa"),
        }
    }

    return (i1, i2, i3);
}

fn only_white_spaces(str: &str) -> bool {
    str.chars().all(char::is_whitespace)
}

// without <br>\n
fn format_normal_line(line: &str) -> String {
    let mut new = String::from(line);

    if let Some(_index) = label_end(line) {
        new = new.replacen(":", ":</lab>", 1); // even though I have index, I use this, because the insertion to index did not work properly with unicode
        new = String::from("<lab>") + &new;
    }
    new = new.replace("[", "<chord>[");
    new = new.replace("]", "]</chord>");
    new = new.replace(";", "<chord>●</chord>");
    new = new.replace("{", "<b>𝄆</b>");
    new = new.replace("}", "<b>𝄇</b>");
    new = new.replace("|", "<chord>|</chord>");
    new = new.replace("1/2", "½");
    new = new.replace("1/3", "⅓");
    new = new.replace("1/4", "¼");
    new = new.replace("2/3", "⅔");
    new = new.replace("3/4", "¾");
    return new;
}

fn label_end(line: &str) -> Option<usize> {
    for (i, c) in line.chars().enumerate() {
        if char::is_whitespace(c) {
            return None;
        }
        if c == ':' {
            return Some(i);
        }
    }
    return None;
}
